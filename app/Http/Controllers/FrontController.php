<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use App\Models\ContactUs;
use App\Models\Portfolio;
use App\Models\Service;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
        $data = Portfolio::find(1);
        $services = Service::where('is_active',1)->get();
        return view('front.index',compact('data','services'));
    }
    public function contactus(){
        $data = ContactUs::find(1);
        return view('front.contactus',compact('data'));
    }
    public function about(){
        $data = AboutUs::find(1);
        return view('front.about',compact('data'));
    }
    public function services(){
        $data = Service::where('is_active',1)->get();
        return view('front.services',compact('data'));
    }
}
