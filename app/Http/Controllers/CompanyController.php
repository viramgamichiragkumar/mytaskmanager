<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\Facades\DataTables;

class CompanyController extends Controller
{
    
    public function __construct()
    {
        Config::set('moduleName','Company');
        Config::set('moduleUrl','admin.company');
    }

    public function index(Request $request){
        
        if ($request->ajax()) {
            $data = Company::select('*');
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('image', function($row){
                        $path = ($row->image && file_exists(storage_path('app/company/'.$row->image))) ? asset('storage/app/company/'.$row->image) : noimage();
                        // $img = '<a href="'.$path.'" data-toggle="lightbox" data-title="Image" data-footer="Image" data-gallery="service-gallery">
                        //     <img src="'.$path.'" class="img-fluid" alt="Image" width="50" height="50" loading="lazy"></a>';
                        $img = '<img src="'.$path.'" class="img-fluid" width="50" height="50" loading="lazy" data-remote="'.$path.'" data-toggle="lightbox" data-title="'.$row->image.'" data-gallery="service-gallery" data-width="640">';
                        return $img;
                    })
                    ->addColumn('is_active', function($row){
                        return _status($row->is_active);
                    })
                    ->addColumn('action', function($row){
                        $url = config('moduleUrl');
                        $btn = '<a href="'.route("$url.edit",encrypt($row->id)).'" class="editBtn mx-1"><i class="fa fa-edit"></i></a>';
                        $btn .= '<a href="'.route("$url.delete",encrypt($row->id)).'" class="deleteBtn mx-1"><i class="fa fa-times"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['image','is_active','action'])
                    ->make(true);
        }

        return view('admin.company.index');
    }

    public function add(){
        return view('admin.company.create');
    }

    public function store(Request $request){
        $request->validate([
            'name'=>'required|string|unique:companies,name',
            'is_active'=>'required',
        ]);

        $filename = NULL;
        if($request->has('image')){
            $file = $request->file('image');
            $loader_name = uniqid('company_', true) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('company/',$filename);
        }

        Company::create([
            'name' => $request->name,
            'image' => $filename,
            'is_active'=>$request->is_active,
        ]);
        
        return redirect('admin/company')->with('success',config('moduleName').' Added Successfully');
    }

    public function edit($id){
        $data = Company::find(decrypt($id));
        return view('admin.company.edit',compact('data'));
    }

    public function update(Request $request){
        $id = decrypt($request->id);
        $request->validate([
            'name'=>'required|string|unique:companies,name,'.$id,
            'is_active'=>'required',
        ]);

        $filename = $request->old_image ?? NULL;
        if($request->has('image')){
            if(!empty($request->old_image)){
                removeFile(storage_path('app/company/'.$request->old_image));
            }
            $file = $request->file('image');
            $filename = uniqid('company_', true) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('company/',$filename);
        }

        $data = Company::find($id);
        $data->update(
        [
            'name' => $request->name,
            'image' => $filename,
            'is_active'=>$request->is_active,
        ]);
        
        return redirect('admin/company')->with('success',config('moduleName').' Updated Successfully');
    }

    public function delete($id,Request $request){
        $data = Company::find(decrypt($id));
        if(!empty($data->image)){
            removeFile(storage_path('app/company/'.$data->image));
        }
        $data->delete();
        if($request->ajax()){
            return response()->json([],200);
        }
        return redirect(route('admin.company'))->with('success',config('moduleName').' Deleted Successfully');
    }
}
