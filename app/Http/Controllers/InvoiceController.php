<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\Facades\DataTables;

class InvoiceController extends Controller
{
     
    public function __construct()
    {
        Config::set('moduleName','Invoice');
        Config::set('moduleUrl','admin.invoice');
    }

    public function index(Request $request){
        
        if ($request->ajax()) {
            $data = Invoice::select('*');
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->editColumn('start_date', function($row){
                        return date('d/m/Y',strtotime($row->start_date));
                    })
                    ->editColumn('due_date', function($row){
                        return date('d/m/Y',strtotime($row->due_date));
                    })
                    ->addColumn('status', function($row){
                        if($row->status == 0){
                            return '<span class="badge badge-danger">Unpaid</span>';
                        }elseif($row->status == 1){
                            return '<span class="badge badge-success">Paid</span>';
                        }
                    })
                    ->addColumn('action', function($row){
                        $url = config('moduleUrl');
                        $btn = '<div style="white-space:nowrap;"><a href="'.route("$url.edit",encrypt($row->id)).'" class="editBtn mx-1"><i class="fa fa-edit"></i></a>';
                        $btn .= '<a href="'.route("$url.delete",encrypt($row->id)).'" class="deleteBtn mx-1"><i class="fa fa-times"></i></a>';
                        $btn .= '<a href="'.route("$url.download",encrypt($row->id)).'" class="downloadBtn mx-1"><i class="fa fa-download"></i></a></div>';

                        return $btn;
                    })
                    ->rawColumns(['status','action'])
                    ->make(true);
        }

        return view('admin.invoice.index');
    }

    public function add(){
        return view('admin.invoice.create');
    }

    public function generateInvoiceNo(){
        $last = Invoice::latest()->first();
        $lastId = ($last) ? $last->id : 1;
        $no = 'inv_'.str_pad($lastId, 3, '0', STR_PAD_LEFT);

        while(Invoice::where('invoice_no',$no)->count()){
            $no = 'inv_'.str_pad($lastId++, 3, '0', STR_PAD_LEFT);
        }
        return $no;
    }
    public function store(Request $request){
        $request->validate([
            "receiver_name" => "required",
            "title" => "required",
            "start_date" => "required|date",
            "due_date" => "required|date",
            "status" => "required|integer",
            "total_amount" => "required|numeric",
        ]);

        $invoiceData = $request->invoice_data;
        $total=0;
        $invoice_data = array_map(function ($date, $description, $amount) use(&$total){
            if($date && $description && $amount){
                $total += $amount;
                return [
                    "date" => $date,
                    "description" => $description,
                    "amount" => $amount,
                ];
            }
            return [];
        }, $invoiceData['date'], $invoiceData['description'], $invoiceData['amount']);
        $invoice_data = array_filter($invoice_data);

        $invoice_no = self::generateInvoiceNo();

        Invoice::create([
            'invoice_no' => $invoice_no,
            'receiver_name' => $request->receiver_name,
            'title' => $request->title,
            'start_date' => $request->start_date,
            'due_date' => $request->due_date,
            'total_amount' => ($request->total_amount >= $total) ? $request->total_amount : $total,
            'invoice_data' => json_encode($invoice_data),
            'status'=>$request->status,
        ]);
        
        return redirect('admin/invoice')->with('success',config('moduleName').' Added Successfully');
    }

    public function edit($id){
        $data = Invoice::find(decrypt($id));
        return view('admin.invoice.edit',compact('data'));
    }

    public function update(Request $request){
        $id = decrypt($request->id);
        $request->validate([
            "receiver_name" => "required",
            "title" => "required",
            "start_date" => "required|date",
            "due_date" => "required|date",
            "status" => "required|integer",
            "total_amount" => "required|numeric",
        ]);

        $invoiceData = $request->invoice_data;
        $total = 0;
        $invoice_data = array_map(function ($date, $description, $amount) use(&$total) {
            if($date && $description && $amount){
                $total += $amount;
                return [
                    "date" => $date,
                    "description" => $description,
                    "amount" => $amount,
                ];
            }
            return [];
        }, $invoiceData['date'], $invoiceData['description'], $invoiceData['amount']);
        $invoice_data = array_filter($invoice_data);

        $data = Invoice::find($id);
        $data->update(
        [
            'receiver_name' => $request->receiver_name,
            'title' => $request->title,
            'start_date' => $request->start_date,
            'due_date' => $request->due_date,
            'total_amount' => ($request->total_amount >= $total) ? $request->total_amount : $total,
            'invoice_data' => json_encode($invoice_data),
            'status'=>$request->status,
        ]);
        
        return redirect('admin/invoice')->with('success',config('moduleName').' Updated Successfully');
    }

    public function delete($id,Request $request){
        $data = Invoice::find(decrypt($id));

        $data->delete();
        if($request->ajax()){
            return response()->json([],200);
        }
        return redirect(route('admin.invoice'))->with('success',config('moduleName').' Deleted Successfully');
    }

    
    public function download_invoice($id,Request $request){
        $id = decrypt($id);
        $invoice = Invoice::find($id);

        $name = "$invoice->title Invoice";

        $date = date('d/m/Y',strtotime($invoice->start_date));
        $invoiceDueDate = date('d/m/Y',strtotime($invoice->due_date));
        // $desc = "Web development services";


        $data = [
            'title' => "$name",
            'date' => $date,
            'receiver_name' => $invoice->receiver_name,
            'invoiceNo' => $invoice->invoice_no,
            'invoiceDate' => $date,
            'invoiceDueDate' => $invoiceDueDate,
            'invoice_data'=>$invoice->invoice_data,
        ];
        
        // $pdf = \PDF::loadView('layouts.invoice-pdf', $data);
        $view = view('layouts.invoice-pdf', $data)->render();
        $pdf = \PDF::loadHTML($view);
        
        return $pdf->download("$name.pdf");
    }
}
