<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Config;

class ServiceController extends Controller
{
    public function __construct()
    {
        Config::set('moduleName','Services');
        Config::set('moduleUrl','admin.services');
    }

    public function index(Request $request){
        
        if ($request->ajax()) {
            $data = Service::select('*');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('image', function($row){
                        $path = $row->image && file_exists(storage_path('app/service/'.$row->image)) ? asset('storage/app/service/'.$row->image) : noimage();
                        // $img = '<a href="'.$path.'" data-toggle="lightbox" data-title="Image" data-footer="Image" data-gallery="service-gallery">
                        //     <img src="'.$path.'" class="img-fluid" alt="Image" width="50" height="50" loading="lazy"></a>';
                        $img = '<img src="'.$path.'" class="img-fluid" width="50" height="50" loading="lazy" data-remote="'.$path.'" data-toggle="lightbox" data-title="'.$row->image.'" data-gallery="service-gallery" data-width="640">';
                        return $img;
                    })
                    ->addColumn('is_active', function($row){
                        return _status($row->is_active);
                    })
                    ->addColumn('action', function($row){
                        $url = config('moduleUrl');
                        $btn = '<a href="'.route("$url.edit",encrypt($row->id)).'" class="editBtn mx-1"><i class="fa fa-edit"></i></a>';
                        $btn .= '<a href="'.route("$url.delete",encrypt($row->id)).'" class="deleteBtn mx-1"><i class="fa fa-times"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['image','is_active','action'])
                    ->make(true);
        }

        return view('admin.services.index');
    }

    public function add(){
        return view('admin.services.create');
    }

    public function store(Request $request){
        $request->validate([
            'name'=>'required|string|unique:services,name',
            'is_active'=>'required',
            'description'=>'required',
        ]);

        $filename = NULL;
        if($request->has('image')){
            $file = $request->file('image');
            $filename = $file->getClientOriginalName().uniqid().'.'.$file->getClientOriginalExtension();;
            $file->storeAs('service/',$filename);
        }

        $service = Service::create([
            'name' => $request->name,
            'image' => $filename,
            'is_active'=>$request->is_active,
            'description'=>$request->description,
        ]);
        
        return redirect('admin/services')->with('success','Service Added Successfully');
    }

    public function edit($id){
        $data = Service::find(decrypt($id));
        return view('admin.services.edit',compact('data'));
    }

    public function update(Request $request){
        $id = decrypt($request->id);
        $request->validate([
            'name'=>'required|string|unique:services,name,'.$id,
            'is_active'=>'required',
            'description'=>'required',
        ]);

        $filename = $request->old_image ?? NULL;
        if($request->has('image')){
            if(!empty($request->old_image)){
                removeFile(storage_path('app/service/'.$request->old_image));
            }
            $file = $request->file('image');
            $filename = uniqid('service_', true) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('service/',$filename);
        }

        $service = Service::find($id);
        $service->update(
        [
            'name' => $request->name,
            'image' => $filename,
            'is_active'=>$request->is_active,
            'description'=>$request->description,
        ]);
        
        return redirect('admin/services')->with('success','Service Updated Successfully');
    }

    public function delete($id,Request $request){
        $service = Service::find(decrypt($id));
        if(!empty($service->image)){
            removeFile(storage_path('app/service/'.$service->image));
        }
        $service->delete();
        if($request->ajax()){
            return response()->json([],200);
        }
        return redirect(route('admin.services'))->with('success','Service Deleted Successfully');
    }
}
