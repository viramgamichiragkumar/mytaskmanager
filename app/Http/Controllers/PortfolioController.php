<?php

namespace App\Http\Controllers;

use App\Models\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;


class PortfolioController extends Controller
{
    
    public function __construct()
    {
        Config::set('moduleName','Portfolio');
        Config::set('moduleUrl','admin.portfolio');
    }
    
    public function index(){
        $data = Portfolio::first();
        return view('admin.portfolio',compact('data'));
    }

    public function portfolio(Request $request){
        $request->validate([
            'name'=>'required',
            'designation'=>'required',
            'aboutme'=>'required',
        ]);

        $filename = $request->old_file ?? NULL;
        $resume = $request->old_resume ?? NULL;

        if($request->has('image')){
            if(!empty($filename)){
                removeFile(storage_path('app/public/'.$filename));
            }
            $file = $request->file('image');
            $filename = 'portfolio_'.uniqid().'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/',$filename);
        }
        
        if($request->has('resume')){
            if(!empty($resume)){
                removeFile(storage_path('app/public/'.$resume));
            }
            $file = $request->file('resume');
            $resume = 'resume_'.$file->getClientOriginalName().uniqid().'.'.$file->getClientOriginalExtension();;
            $file->storeAs('public/',$resume);
        }

        $data = [
            'name' => $request->name,
            'designation' => $request->designation,
            'aboutme' => $request->aboutme,
            'image' => $filename,
            'resume' => $resume,
        ];
        
        // if (!Portfolio::count()) {
            $data['slug'] = \Str::slug($request->name);
        // }
        
        Portfolio::updateOrCreate(['id' => 1], $data);
        return redirect()->route('admin.portfolio')->with('success',config('moduleName').' Update Successfully');
    }
}
