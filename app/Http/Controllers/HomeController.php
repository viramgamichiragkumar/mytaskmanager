<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use App\Models\Company;
use App\Models\ContactUs;
use App\Models\Inquiry;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count = [
            'users'=> User::count(),  
            'services'=> Service::where('is_active',1)->count(),  
            'company'=> Company::where('is_active',1)->count(),  
            'inquiry'=> Inquiry::count(),  
        ];

        return view('home',$count);
    }

    public function lock_screen(Request $request){
        if(session('lock') != 1){
            return redirect()->route('home');
        }

        if($request->method() == 'POST'){
            if(Hash::check($request->password,auth()->user()->password)){
                session(['last_activity' => time(),'lock'=>0]);
                $redirect = session()->get('redirect_to') ?? 'admin';
                session()->forget('redirect_to');
                return redirect(url("/$redirect"));
            }
            return back()->with('error','something went wrong...');
        }
        return view('auth.lock_screen');
    }
    
    public function contactus(Request $request){
        if($request->method() == 'POST'){
            ContactUs::updateOrCreate(
            ['id'=>1],
            [
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
            ]);
            return redirect()->route('admin.contactus')->with('success','Contact updated successfully :)');
        }
        $contact = ContactUs::first();
        return view('admin.contactus',compact('contact'));
    }

    public function aboutus(Request $request){
        if($request->method() == 'POST'){

            $data = [
                'vision' => $request->vision,
                'mission' => $request->mission,
            ];

            if($request->has('image')){
                if(!empty($request->old_file)){
                    removeFile(storage_path('app/public/'.$request->old_file));
                }
                $file = $request->file('image');
                $filename = uniqid('about_', true) . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/',$filename);
                $data['image'] = $filename;
            }

            AboutUs::updateOrCreate(['id'=>1],$data);
            return redirect()->route('admin.aboutus')->with('success','Data updated successfully :)');
        }
        $about = AboutUs::first();
        return view('admin.aboutus',compact('about'));
    }

    public function profile(Request $request){
        $user = User::find(Auth::user()->id);
        $count = [
            'services'=> Service::where('is_active',1)->count(),  
            'company'=> Company::where('is_active',1)->count(),  
            'inquiry'=> Inquiry::count(),  
        ];

        if($request->method() == 'POST'){
            $request->validate([
                'name'=>'required',
                'email'=>'required|unique:users,email,'.$user->id,
                'password'=> 'sometimes',
                'confirmpassword'=> 'sometimes|required_with:password',
            ]);

            $filename = $request->old_image ?? NULL;
            $pass = $user->password;

            if($request->has('image')){
                if(!empty($request->old_image)){
                    removeFile(storage_path('app/profile/'.$request->old_image));
                }
                $file = $request->file('image');
                $filename = $file->getClientOriginalName().uniqid().'.'.$file->getClientOriginalExtension();;
                $file->storeAs('profile/',$filename);
            }
            
            if(!empty($request->password) && !empty($request->confirmpassword)){
                if($request->password == $request->confirmpassword){
                    $pass = Hash::make($request->password);
                }
                session(['lock'=>1]);
            }
            $user->update([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>$pass,
                'image'=>$filename,
            ]);
            return redirect()->route('admin.profile')->with('success','Profile Updated Successfully');
        }
        return view('admin.profile',$count,compact('user'));
    }
}
