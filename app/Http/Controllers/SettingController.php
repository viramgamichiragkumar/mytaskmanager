<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

// use Image;

require_once("public/PHPImageWorkshop/src/ImageWorkshop.php");
require_once('public/PHPImageWorkshop/src/Core/ImageWorkshopLayer.php');
require_once('public/PHPImageWorkshop/src/Exception/ImageWorkshopException.php');
require_once('public/PHPImageWorkshop/src/Core/ImageWorkshopLib.php');
use PHPImageWorkshop\ImageWorkshop;

class SettingController extends Controller
{
    public function index(Request $request){
        $setting = Setting::first();
        if($request->method() == 'POST'){

            $request->validate([
                "sitename" => "required|string",
                // "logo" => "mimes:png,jpg,jpeg",
                // "loader" => "mimes:png,jpg,jpeg",
                // "favicon" => "mimes:png,jpg,jpeg",
            ]);

            if($setting){
                $logo_name = $setting->logo;
                $loader_name = $setting->loader;
                $favicon_name = $setting->favicon;
            }else{
                $logo_name = $loader_name = $favicon_name = '';
            }

            if($request->has('logo')){
                if($logo_name){
                    removeFile(storage_path('app/setting/'.$logo_name));
                }

                $file = $request->file('logo');
                $logo_name = uniqid('logo_', true) . '.' . $file->getClientOriginalExtension();
                $file->storeAs('setting/',$logo_name);
               

                /*$layer = ImageWorkshop::initFromPath($path);
                
                // watermark
                $wwfLogoLayer = ImageWorkshop::initFromPath(asset('assets/dist/img/photo1.png'));
                $tuxLayer = ImageWorkshop::initFromPath(asset('assets/dist/img/photo2.png'));	
                $layer->addLayerOnTop($wwfLogoLayer, 20, 10, 'LB');
                $layer->addLayerOnTop($tuxLayer, 20, 10, 'RT'); 
                $layer->applyFilter(IMG_FILTER_NEGATE);

                $layer->save('storage/app/setting/', $logo_name, true, null, 95);*/
            }
            if($request->has('loader')){
                if($loader_name){
                    removeFile(storage_path('app/setting/'.$loader_name));
                }
                $file = $request->file('loader');
                $loader_name = uniqid('loader_', true) . '.' . $file->getClientOriginalExtension();
                $file->storeAs('setting/',$loader_name);
            }
            if($request->has('favicon')){
                if($favicon_name){
                    removeFile(storage_path('app/setting/'.$favicon_name));
                }
                $file = $request->file('favicon');
                $favicon_name = uniqid('favicon_', true) . '.' . $file->getClientOriginalExtension();
                $file->storeAs('setting/',$favicon_name);
            }

            $social = [
                "facebook" => $request->facebook,
                "linkedin" => $request->linkedin,
                "instagram" => $request->instagram,
                "twitter" => $request->twitter,
            ];

            $setting = Setting::updateOrCreate(['id'=>1],[
                "sitename" =>$request->sitename,
                "logo" =>$logo_name,
                "loader" =>$loader_name,
                "favicon" =>$favicon_name,
                "social" =>json_encode($social),
            ]);
            
            Config::set('site_setting', NULL);
            return redirect()->route('admin.setting')->with('success', 'Setting Updated successfully.');
        }
        
        return view('admin.setting',compact('setting'));
    }
    
}
