<?php

namespace App\Http\Controllers;

use App\Models\Inquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\Facades\DataTables;

class InquiryController extends Controller
{
    
    public function __construct()
    {
        Config::set('moduleName','Inquiry');
        Config::set('moduleUrl','admin.inquiry');
    }

    public function index(Request $request){
        
        if ($request->ajax()) {
            $data = Inquiry::select('*');
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                        if($row->status == 0){
                            return '<span class="badge badge-info">Pending</span>';
                        }elseif($row->status == 1){
                            return '<span class="badge badge-secondary">Archived</span>';
                        }elseif($row->status == 2){
                            return '<span class="badge badge-primary">Confirmed</span>';
                        }elseif($row->status == 3){
                            return '<span class="badge badge-danger">Decline</span>';
                        }elseif($row->status == 4){
                            return '<span class="badge badge-success">Finished</span>';
                        }
                    })
                    ->addColumn('action', function($row){
                        $url = config('moduleUrl');
                        $btn = '<a href="'.route("$url.edit",encrypt($row->id)).'" class="editBtn mx-1"><i class="fa fa-edit"></i></a>';
                        $btn .= '<a href="'.route("$url.delete",encrypt($row->id)).'" class="deleteBtn mx-1"><i class="fa fa-times"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['status','action'])
                    ->make(true);
        }

        return view('admin.inquiry.index');
    }

    public function add(){
        return view('admin.inquiry.create');
    }

    public function store(Request $request){
        $request->validate([
            'name'=>'required|string',
            'email'=>'required|email',
            'phone'=>'required|numeric',
            'subject'=>'required',
            'message'=>'required',
            'status'=>'required',
        ]);

        Inquiry::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
            'subject' => $request->subject,
            'status'=>$request->status,
        ]);
        
        return redirect('admin/inquiry')->with('success',config('moduleName').' Added Successfully');
    }

    public function edit($id){
        $data = Inquiry::find(decrypt($id));
        return view('admin.inquiry.edit',compact('data'));
    }

    public function update(Request $request){
        $id = decrypt($request->id);
        $request->validate([
            'name'=>'required|string',
            'email'=>'required|email',
            'phone'=>'required|numeric',
            'subject'=>'required',
            'message'=>'required',
            'status'=>'required',
        ]);


        $data = Inquiry::find($id);
        $data->update(
        [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
            'subject' => $request->subject,
            'status'=>$request->status,
        ]);
        
        return redirect('admin/inquiry')->with('success',config('moduleName').' Updated Successfully');
    }

    public function delete($id,Request $request){
        $data = Inquiry::find(decrypt($id));

        $data->delete();
        if($request->ajax()){
            return response()->json([],200);
        }
        return redirect(route('admin.inquiry'))->with('success',config('moduleName').' Deleted Successfully');
    }

    
    public function inquiry_front(Request $request){
        $request->validate([
            'name'=>'required|string',
            'email'=>'required|email',
            'phone'=>'required|numeric',
            'subject'=>'required',
            'message'=>'required',
        ]);
        Inquiry::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
            'subject' => $request->subject,
            'status'=> 0 ,
        ]);
        
        return redirect()->back()->with('success','Thank You For Contacting us. Our Team Contact you soon.');
    }

}
