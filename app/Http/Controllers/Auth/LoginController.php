<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
    public function handleGoogleCallback()
    {
        try {
      
            $user = Socialite::driver('google')->user();
       
            $finduser = User::where('google_id', $user->id)->first();
       
            if($finduser){
       
                Auth::login($finduser);
      
                return redirect()->intended('admin');
       
            }else{
                if(!User::where('email', $user->email)->exists()){
                    $newUser = User::create([
                        'name' => $user->name,
                        'email' => $user->email,
                        'login_typ' => 1,
                        'google_id'=> $user->id,
                        'password' => Hash::make('123456')
                    ]);
        
                    $file = md5($newUser->id).'.jpg';
                    $user_imge = file_get_contents($user->avatar);
                    $dest = Storage::put('profile/'.$file, $user_imge);
                    
                    $newUser->image = $file;
                    $newUser->save();
                }else{
                    $newUser = User::where('email', $user->email)->first();
                }
                Auth::login($newUser);
      
                return redirect()->intended('admin');
            }
      
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

}
