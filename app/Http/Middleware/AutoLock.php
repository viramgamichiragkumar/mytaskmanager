<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AutoLock
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $timeout = 15; // Timeout in minutes

        if (Auth::check()) {
            $lastActivity = session('last_activity');
            if(!$request->ajax()){
                // Check if last activity exists and check timeout

                if (session('lock') || ($lastActivity && time() - $lastActivity > $timeout * 60)) {
                    ($request->method() == 'GET') ? session(['redirect_to'=>$request->path()]) : '';
                    session(['lock'=>1]);
                    return redirect()->route('lock-screen');
                }
            }
            session(['last_activity' => time()]);
        }

        return $next($request);
    }
}
