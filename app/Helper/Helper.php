<?php

// namespace App\Helper;

// use SebastianBergmann\CodeUnit\FunctionUnit;

// class Helper{

use App\Models\AboutUs;
use App\Models\ContactUs;
use Illuminate\Support\Facades\File;

    function removeFile($filepath = ''){
        $fileExists = (bool) @get_headers($filepath)[0];
        if((!empty($filepath)) && ($fileExists || file_exists($filepath))){
            try{
                unlink($filepath);
            }catch(Exception $e){
                File::delete($filepath);
            }
        }
        return;
    }

    function setting($col = ''){
        $setting = config('site_setting');
        return ($setting && $col) ? $setting[$col] : $setting;
    }
    
    function noimage($flag = 0){
        return ($flag) ? asset('public/assets/dist/img/user2-160x160.jpg') : asset('public/'.env('IMG_DEFAULT'));
    }

    function _status($status = 0){
        return ($status) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">In-Active</span>';
    }

    function _selected($option,$value){
        return ($option == $value) ? 'selected' : "";
    }

    function contactUs($col = ''){
        $contactUs = ContactUs::when($col,function($q) use($col){
            $q->select([$col]);
        })->first();
        return ($contactUs && $col) ? $contactUs[$col] : $contactUs;
    }

    function aboutUs(){
        $aboutUs = AboutUs::first();
        return $aboutUs;
    }

// }
?>