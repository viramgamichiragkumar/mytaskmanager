<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

use App\Models\Setting;
use Exception;
use Illuminate\Support\Collection;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (is_null(config('site_setting'))) {
            $this->app->booted(function () {
                try{
                    $siteSetting = Setting::firstOrFail();
                    Config::set('site_setting', $siteSetting->toArray());

                    Config::set('app.name', ucfirst($siteSetting->sitename));
                }catch(Exception $e){}
            });
        }

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
