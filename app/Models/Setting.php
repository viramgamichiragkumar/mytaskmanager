<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    public $guarded = [];

    public function getSocialAttribute($value)
    {
        return json_decode($value, true);
    }
}
