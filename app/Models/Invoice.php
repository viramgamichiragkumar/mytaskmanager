<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    public $guarded = [];

    
    public function getInvoiceDataAttribute($value)
    {
        return json_decode($value, true);
    }
}
