<!DOCTYPE html>
<html>
<head>
    {{-- <meta http-equiv="refresh" content="5"> --}}
    <title>INVOICE</title>
    <style>
    *{font-family: calibri;} table tr td, th {padding: 5px;}
    .space-between{display:flex;justify-content: space-between;}
    .table-div{display: inline-block;width: 100%;margin: 2% 0px;}
    .fixed-footer{position: fixed;left: 0;bottom: 0;width: 100%;background-color: lightgray;}
    </style>
</head>
<body>
    <h1><center>INVOICE</center></h1>

    <div style="margin: 2% 0px;">
        <div style="display: flex;width: 100%;justify-content: end;">
            <div>
                <p><b>Invoice number:</b> 002</p>
                <p><b>Invoice date:</b> 09/03/2023</p>
                <p><b>Due date:</b> 10/03/2023</p>
            </div>
        </div>
        <div><b>Focus awards</b></div>
    </div>

    {{-- <h1>{{ $title }}</h1>
    <p>{{ $date }}</p> --}}

    <div class="table-div">
        <table border="1" width="100%" style="border-collapse:collapse">
            <thead>
                <tr align="right">
                    <th width="40%">Description</th>
                    <th width="30%">Date</th>
                    <th width="30%">Total</th>
                </tr>
            </thead>
            <tbody>
                <tr align="right">
                    <td>Web development services</td>
                    <td>10/03/2023</td>
                    <td>20,000 ₹</td>
                </tr>
            </tbody>
        </table>

        <div style="width:30%;float: right;">
            <div class="space-between" style="border-top:1px gray solid;margin:4% 0px;padding:5px;">
                <b>Total </b>
                <span>20,000 ₹</span>
            </div>
        </div>
    </div>

    <div>
        <p>Thank you for your business. Please contact us with any questions regarding this Invoice.</p>
        <hr>
        <div class="space-between">
            <p><b>Name :</b> Viramgami Chiragkumar</p>
            <p><b>Ph :</b> +919409699604</p>
        </div>
    </div>
</body>
</html>