<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name','ADMIN')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    @include('layouts.partials.headerscript')

</head>
@section('project_title', config('app.name'))

@if(auth()->check())
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            {{-- header --}}
            @include('layouts.partials.header')

            {{-- sidebar --}}
            @include('layouts.partials.sidebar')


            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">Dashboard</h1>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active">Dashboard v1</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">


                    @yield('content')


                </section>
                <!-- /.content -->
            </div>
        </div>
            <!-- /.content-wrapper -->

            @include('layouts.partials.footer')

            <!-- ./wrapper -->

            @include('layouts.partials.footerscript')

    </body> 
@else

    @yield('content')

@endif

</html>
