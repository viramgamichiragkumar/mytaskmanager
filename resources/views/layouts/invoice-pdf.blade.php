<!DOCTYPE html>
<html style="font-family: sans-serif;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $title }} - INVOICE</title>
    <style>
        h1 {
            text-align: center;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
        }
        .right-align {
            text-align: right;
        }
        .total-row {
            display: flex;
            justify-content: space-between;
            border-top: 1px gray solid;
            margin: 4% 0px;
            padding: 5px;
        }
    </style>
</head>
<body>
    <h1>INVOICE</h1>

    <div style="margin: 5% 0px;">
        <div style="text-align: right;">
            <p><b>Invoice number:</b> {{ $invoiceNo }}</p>
            <p><b>Invoice date:</b> {{ $invoiceDate }}</p>
            <p><b>Due date:</b> {{ $invoiceDueDate }}</p>
        </div>
        <div><b>{{ $receiver_name ?? 'Focus awards' }}</b></div>
    </div>

    <div style="display: inline-block; width: 100%; margin: 2% 0px;">
        <table border="1">
            <thead>
                <tr class="right-align" align="right">
                    <th width="40%">Description</th>
                    <th width="30%">Date</th>
                    <th width="30%">Total</th>
                </tr>
            </thead>
            <tbody>
                @php($totalAmount = 0)
                @foreach ($invoice_data as $row)
                    <tr class="right-align">
                        <td>{{ $row['description'] }}</td>
                        <td>{{ date('d/m/Y',strtotime($row['date'])) }}</td>
                        <td>{{ number_format($row['amount']) }} <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span></td>
                    </tr>
                    @php($totalAmount += $row['amount'])
                @endforeach
            </tbody>
        </table>


        <div style="width: 30%; float: right;">
            <div class="total-row">
                <b style="float: left;">Total</b>
                <span style="float: right;">{{ number_format($totalAmount) }} <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span></span>
            </div>
        </div>
    </div>

    <div style="margin-top:10%;">
        <p>Thank you for your business. Please contact us with any questions regarding this Invoice.</p>
        <hr>
        <div style="overflow: auto;">
            <p style="float: left;"><b>Name:</b> Viramgami Chiragkumar</p>
            <p style="float: right;"><b>Ph:</b> +919409699604</p>
        </div>
    </div>
</body>
</html>
