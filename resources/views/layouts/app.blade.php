<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'ADMIN') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.partials.headerscript')
    <style>
        .colored-toast.swal2-icon-success {
            /* background-color: #a5dc86 !important; */
            background-color: #39600d !important;
        }

        .colored-toast.swal2-icon-error {
            background-color: #823434 !important;
        }

        .colored-toast.swal2-icon-warning {
            background-color: #f8bb86 !important;
        }

        .colored-toast.swal2-icon-info {
            background-color: #3fc3ee !important;
        }

        .colored-toast.swal2-icon-question {
            background-color: #87adbd !important;
        }

        .colored-toast .swal2-title {
            color: white;
        }

        .colored-toast .swal2-close {
            color: white;
        }

        .colored-toast .swal2-html-container {
            color: white;
        }
    </style>
    <style>
        .ekko-lightbox .modal-dialog {
          display: flex !important;
          align-items: center;
          justify-content: center;
          max-width: 50vw !important; /* Adjust the maximum width of the modal as needed */
          max-height: 50vh !important; /* Adjust the maximum height of the modal as needed */
        }
      
        .ekko-lightbox .modal-dialog img {
          max-width: 100% !important;
          max-height: 100% !important;
          /* width: auto !important; */
          /* height: auto !important; */
        }
      </style>
</head>
@section('project_title', config('app.name'))
@if (auth()->check() && !session()->get('lock'))

    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            {{-- header --}}
            @include('layouts.partials.header')

            {{-- sidebar --}}
            @include('layouts.partials.sidebar')


            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        @if (isset($moduleName))
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0 text-dark">{{ $moduleName }}</h1>
                                </div><!-- /.col -->
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                                        <li class="breadcrumb-item active">{{ $moduleName }}</li>
                                    </ol>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        @endif
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">

                    @yield('content')


                </section>
                <!-- /.content -->
            </div>
        </div>
        <!-- /.content-wrapper -->

        @include('layouts.partials.footer')

        <!-- ./wrapper -->

        @include('layouts.partials.footerscript')

    </body>
@else
    @yield('content')
    @include('layouts.partials.footerscript')
@endif

</html>
@yield('script')

<script>

    function previewImage(input, previewElement) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(previewElement).attr('src', e.target.result);
                $(previewElement).show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    var table;
    function server_datatable(ajaxUrl,cols){
        table = $('.server_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: ajaxUrl,
            columns: cols
        });
    }

    var toastMixin = Swal.mixin({
        toast: true,
        icon: 'success',
        title: 'General Title',
        animation: false,
        position: 'top-right',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    // document.querySelector(".first").addEventListener('click', function() {
    //     Swal.fire({
    //         toast: true,
    //         icon: 'success',
    //         title: 'Posted successfully',
    //         animation: false,
    //         position: 'bottom',
    //         showConfirmButton: false,
    //         timer: 3000,
    //         timerProgressBar: true,
    //         didOpen: (toast) => {
    //             toast.addEventListener('mouseenter', Swal.stopTimer)
    //             toast.addEventListener('mouseleave', Swal.resumeTimer)
    //         }
    //     })
    // });

    @if (session()->has('success'))
        toastMixin.fire({
            animation: true,
            iconColor: 'white',
            customClass: {
                popup: 'colored-toast'
            },
            title: "{{ session()->get('success') }}",
        });

        /* $(document).Toasts('create', {
            title: 'Success',
            body: '{{ session()->has('success') }}'
        }) */

    @endif

    @if (session()->has('error'))
        toastMixin.fire({
            animation: true,
            iconColor: 'white',
            customClass: {
                popup: 'colored-toast'
            },
            icon: 'error',
            title: "{{ session()->get('error') }}",
        });
    @endif


$(document).ready(function () {
    $(document).on('click','[type="submit"]',function(){
        $(this).attr('disabled',true);
        $(this).closest('form').submit()
    });

/*     $('.ckeditor').each(function () {
        CKEDITOR.replace(this, {
        allowedContent: true,
        fullPage: true,
        height: '300px'
        });
    }); */

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            wrapping: false,
            alwaysShowClose: true,
            showArrows:true,
            onShown: function() {
                // console.log('Checking our the events huh?');
            },
            onContentLoaded: function() {
                var img = $(this).find('.ekko-lightbox-container .ekko-lightbox-item img');
                img.css({
                'width': 'auto',
                'height': '80vh',
                'max-width': '100%',
                'max-height': '100%' // Adjust the maximum height as per your requirement
                });
            }
        });
    });

    $(document).on('click', '.deleteBtn', function(e) {
        e.preventDefault();
        var deleteUrl = $(this).attr('href');
        
        if (confirm("Are you sure you want to delete?")) {

            $.ajax({
            url: deleteUrl,
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
            },
            success: function(response) {
                try {
                   table.draw();
                } catch (error) {
                    location.reload();
                }
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
            });
        }
    });

});
    

</script>
