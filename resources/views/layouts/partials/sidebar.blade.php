<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="{{ (setting('logo')) ? url('storage/app/setting/'.setting('logo')) : asset('public/assets/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light d-table-cell">@yield('project_title')</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ (auth()->user()->image) ? url('storage/app/profile/'.auth()->user()->image) : noimage(1) }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="{{ route('admin.profile') }}" class="d-block">{{ auth()->user()->name }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="{{ route('home') }}" class="nav-link @if(Route::is('home')) active @endif">
            <i class="far fa-circle nav-icon"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.portfolio') }}" class="nav-link @if(Route::is('admin.portfolio')) active @endif">
            <i class="far fa-contact-book nav-icon"></i>
            <p>Portfolio</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.services') }}" class="nav-link @if(Route::is('admin.services')) active @endif">
            <i class="fa fa-archive nav-icon"></i>
            <p>Services</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.company') }}" class="nav-link @if(Route::is('admin.company')) active @endif">
            <i class="far fa-building nav-icon"></i>
            <p>Companies</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.inquiry') }}" class="nav-link @if(Route::is('admin.inquiry')) active @endif">
            <i class="nav-icon fa-solid fa-users"></i>
            <p>
              Inquiry
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.invoice') }}" class="nav-link @if(Route::is('admin.invoices')) active @endif">
            <i class="far fa-file nav-icon"></i>
            <p>Invoices</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.contactus') }}" class="nav-link @if(Route::is('admin.contactus')) active @endif">
            <i class="nav-icon fa-solid fa-users"></i>
            <p>
              Contact Us
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.aboutus') }}" class="nav-link @if(Route::is('admin.aboutus')) active @endif">
            <i class="nav-icon fa-solid fa-address-book"></i>
            <p>
              About Us
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.setting') }}" class="nav-link @if(Route::is('admin.setting')) active @endif">
            <i class="nav-icon fa-solid fa-cog"></i>
            <p>
              Settings
            </p>
          </a>
        </li>


        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Layout Options
              <i class="fas fa-angle-left right"></i>
              <span class="badge badge-info right">6</span>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Top Navigation</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Collapsed Sidebar</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-header">EXAMPLES</li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon far fa-calendar-alt"></i>
            <p>
              Calendar
              <span class="badge badge-info right">2</span>
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
