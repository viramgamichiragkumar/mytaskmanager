<!-- Want To Work Start -->
<section class="wantToWork-area {{-- w-padding2 --}} py-5">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-xl-6 col-lg-8 col-md-8">
                <div class="wantToWork-caption wantToWork-caption2">
                    <h2>Dont worry for contact i`m available</h2>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3">
                <a href="{{ url('/') }}#contact-info-area" class="btn btn-black f-right">Contact Me Now</a>
            </div>
        </div>
    </div>
</section>
<!-- Want To Work End -->