@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Add '.config('moduleName')) }}</div>


                <form action="{{ route(config('moduleUrl').'.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body row">
                        <div class="form-group col-6">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ old('name') }}">
                            @error('name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter email" value="{{ old('email') }}">
                            @error('email')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Phone</label>
                            <input type="number" class="form-control" name="phone" placeholder="Enter phone" value="{{ old('phone') }}" min="0">
                            @error('phone')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="0">Pending</option>
                                <option value="1">Archived</option>
                                <option value="2">Confirmed</option>
                                <option value="3">Lost/Decline</option>
                                <option value="4">Finished</option>
                            </select>
                            @error('status')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        
                        <div class="form-group col-6">
                            <label for="">Subject</label>
                            <input type="text" class="form-control" name="subject" placeholder="Enter subject" value="{{ old('subject') }}">
                            @error('subject')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                        <label for="">Message</label>
                            <textarea class="form-control" name="message" placeholder="Enter message">{{ old('message') }}</textarea>
                            @error('message')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection