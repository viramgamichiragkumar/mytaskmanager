@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3">{{ __('Setting') }}</h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            <li class="nav-item">
                                <a class="nav-link active" href="#tab_1" data-toggle="tab">Site Setting</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab_2" data-toggle="tab">Social</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab_3" data-toggle="tab">Tab 3</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        
                        <form action="{{ route('admin.setting') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            
                            <div class="tab-content">
                                
                                <div class="tab-pane active" id="tab_1">

                                        <div class="form-group">
                                            <label for="sitename">Site Name</label>
                                            <input type="text" class="form-control" name="sitename"
                                                placeholder="Enter site name" value="{{ $setting->sitename ?? env('APP_NAME') }}">
                                        </div>

                                        <div class="form-group d-flex">
                                            <div class="col-6 p-0">
                                                <label for="logo">Logo</label>
                                                <input type="file" class="form-control" name="logo" id="logo"
                                                    placeholder="Enter logo" value="{{ $setting->logo ?? '' }}"
                                                    accept="image/*">
                                            </div>
                                            <div class="col-6">
                                                <img id="logo-preview"
                                                    src="{{ ($setting->logo && file_exists(storage_path('app/setting/' . $setting->logo))) ? asset('storage/app/setting/' . $setting->logo) : noimage() }}"
                                                    alt="Logo Preview" height="100px" width="100px">
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <div class="col-6 p-0">
                                                <label for="loader">Loader</label>
                                                <input type="file" class="form-control" name="loader" id="loader"
                                                    placeholder="Enter loader" value="{{ $setting->loader ?? '' }}"
                                                    accept="image/*">
                                            </div>
                                            <div class="col-6">
                                                <img id="loader-preview"
                                                    src="{{ ($setting->loader && file_exists(storage_path('app/setting/' . $setting->loader))) ? asset('storage/app/setting/' . $setting->loader) : noimage() }}"
                                                    alt="Loader Preview" height="100px" width="100px">
                                            </div>

                                        </div>
                                        <div class="form-group d-flex">
                                            <div class="col-6 p-0">
                                                <label for="favicon">Favicon</label>
                                                <input type="file" class="form-control" name="favicon" id="favicon"
                                                    placeholder="Enter favicon" value="{{ $setting->favicon ?? '' }}"
                                                    accept="image/*">
                                            </div>
                                            <div class="col-6">
                                                <img id="favicon-preview"
                                                    src="{{ ($setting->favicon && file_exists(storage_path('app/setting/' . $setting->favicon))) ? asset('storage/app/setting/' . $setting->favicon) : noimage() }}"
                                                    alt="Favicon Preview" height="100px" width="100px">
                                            </div>

                                        </div>
                                </div>

                                <div class="tab-pane" id="tab_2">

                                    <input type="hidden" name="tab" value="social">
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="facebook">Facebook</label>
                                            <input type="url" class="form-control" name="facebook"
                                                placeholder="Enter Url" value="{{ $setting->social['facebook'] ?? 'https://facebook.com' }}">
                                        </div>

                                        <div class="form-group col-6">
                                            <label for="linkedin">LinkedIn</label>
                                            <input type="url" class="form-control" name="linkedin" id="linkedin"
                                                placeholder="Enter Url" value="{{ $setting->social['linkedin'] ?? 'https://linkedin.com' }}">
                                        </div>

                                        <div class="form-group col-6">
                                            <label for="instagram">Instagram</label>
                                            <input type="url" class="form-control" name="instagram" id="instagram"
                                                placeholder="Enter Url"
                                                value="{{ $setting->social['instagram'] ?? 'https://instagram.com' }}">
                                        </div>

                                        <div class="form-group col-6">
                                            <label for="twitter">Twitter</label>
                                            <input type="url" class="form-control" name="twitter" id="twitter"
                                                placeholder="Enter Url" value="{{ $setting->social['twitter'] ?? 'https://twitter.com' }}">
                                        </div>  
                                    </div>

                                </div>

                                <div class="tab-pane" id="tab_3">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type
                                    specimen book.
                                    It has survived not only five centuries, but also the leap into electronic
                                    typesetting,
                                    remaining essentially unchanged. It was popularised in the 1960s with the
                                    release of Letraset
                                    sheets containing Lorem Ipsum passages, and more recently with desktop
                                    publishing software
                                    like Aldus PageMaker including versions of Lorem Ipsum.
                                </div>
                            </div>
                            
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#logo').change(function() {
            previewImage(this, '#logo-preview');
        });

        $('#loader').change(function() {
            previewImage(this, '#loader-preview');
        });

        $('#favicon').change(function() {
            previewImage(this, '#favicon-preview');
        });
    </script>
@endsection
