@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __(config('moduleName')) }}</div>


                    <form action="{{ route(config('moduleUrl').'.update') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body row">
                            <div class="form-group col-6">
                                <label for="">Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $data->name ?? '' }}">
                            </div>
                            <div class="form-group col-6">
                                <label for="">Designation</label>
                                <input type="text" name="designation" class="form-control" value="{{ $data->designation ?? '' }}">
                            </div>
                            <div class="form-group col-6">
                                <label for="">Resume</label>
                                <input type="file" class="form-control" name="resume" id="resume"
                                        placeholder="Enter resume" value="{{ $data->resume ?? '' }}"
                                        accept=".docx, .doc, .pdf">
                                    <input type="hidden" name="old_resume" value="{{ $data->resume ?? '' }}">
                            </div>
                            <div class="form-group col-6 d-flex">
                                <div class="col-6 p-0">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control" name="image" id="image"
                                        placeholder="Enter image" value="{{ $data->image ?? '' }}"
                                        accept="image/*">
                                    <input type="hidden" name="old_file" value="{{ $data->image ?? '' }}">
                                </div>
                                <div class="col-6">
                                    <img id="image-preview"
                                        src="{{ ($data->image && file_exists(storage_path('app/public/' . $data->image))) ? asset('storage/app/public/' . $data->image) : noimage() }}"
                                        alt="image Preview" height="100px" width="100px">
                                </div>
                            </div>

                            <div class="form-group col-12">
                                <label for="">Aboutme</label>
                                <textarea class="form-control ckeditor" name="aboutme">{{ $data->aboutme ?? '' }}</textarea>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('script')
        <script>
            $('#image').change(function() {
                previewImage(this, '#image-preview');
            });
        </script>
    @endsection
@endsection
