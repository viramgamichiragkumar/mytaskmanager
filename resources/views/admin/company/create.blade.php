@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Add '.config('moduleName')) }}</div>


                <form action="{{ route(config('moduleUrl').'.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body row">
                        <div class="form-group col-6">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ old('name') }}">
                            @error('name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Status</label>
                            <select name="is_active" id="is_active" class="form-control">
                                <option value="1">Active</option>
                                <option value="0">In-Active</option>
                            </select>
                            @error('is_active')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form-group col-6 d-flex">
                            <div class="col p-0">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" name="image" id="image" placeholder="Enter image" accept="image/*">
                            </div>
                            <div class="col-3">
                                <img id="image-preview" src="{{ noimage() }}" alt="image Preview" height="100px" width="100px">
                            </div>
                            @error('image')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    @section('script')
        <script>
            $('#image').change(function() {
                previewImage(this, '#image-preview');
            });
        </script>
    @endsection
@endsection