@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __(config('moduleName')) }}
                    <div class="float-right">
                        <a href="{{ route(config('moduleUrl').".add") }}" class="btn btn-primary">Add</a>
                    </div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table server_table w-100">
                        <thead>
                            <tr>
                                <th>Sr no.</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
    $(function () {
        let ajaxUrl =  "{{ route(config('moduleUrl')) }}";
        let cols = [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'image', name: 'image'},
            {data: 'is_active', name: 'is_active'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ];
        server_datatable(ajaxUrl,cols);
    });
    </script>
@endsection
@endsection