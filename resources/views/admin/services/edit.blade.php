@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Add '.config('moduleName')) }}</div>


                <form action="{{ route(config('moduleUrl').'.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body row">
                        <input type="hidden" class="form-control" name="id" value="{{ encrypt($data->id) }}">
                        <div class="form-group col-6">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ old('name',$data->name) }}">
                            @error('name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Status</label>
                            <select name="is_active" id="is_active" class="form-control">
                                <option value="1" {{ _selected(1,$data->is_active) }}>Active</option>
                                <option value="0" {{ _selected(9,$data->is_active) }}>In-Active</option>
                            </select>
                            @error('is_active')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form-group col-6 d-flex">
                            <div class="col p-0">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" name="image" id="image" placeholder="Enter image" accept="image/*">
                                <input type="hidden" class="form-control" name="old_image" value="{{ $data->image }}">
                            </div>
                            <div class="col-3">
                                <img id="image-preview" src="{{ ($data->image && file_exists(storage_path('app/service/' . $data->image))) ? asset('storage/app/service/'.$data->image) : noimage() }}" alt="image Preview" height="100px" width="100px">
                            </div>
                            @error('image')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Description</label>
                            <textarea class="form-control" name="description" placeholder="Enter description">{{ old('description',$data->description) }}</textarea>
                            @error('description')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    @section('script')
        <script>
            $('#image').change(function() {
                previewImage(this, '#image-preview');
            });
        </script>
    @endsection
@endsection