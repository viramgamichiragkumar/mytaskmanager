@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Add '.config('moduleName')) }}</div>

                <form action="{{ route(config('moduleUrl').'.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body row">
                        <div class="form-group col-6">
                            <label for="">Receiver</label>
                            <input type="text" class="form-control" name="receiver_name" placeholder="Enter receiver name" value="{{ old('receiver_name') }}">
                            @error('receiver_name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Title</label>
                            <input type="text" class="form-control" name="title" placeholder="Enter title" value="{{ old('title') }}">
                            @error('title')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Start Date</label>
                            <input type="date" class="form-control" name="start_date" placeholder="Enter start date" value="{{ old('start_date') }}">
                            @error('start_date')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Due date</label>
                            <input type="date" class="form-control" name="due_date" placeholder="Enter due date" value="{{ old('due_date') }}">
                            @error('due_date')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label for="">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="0">Unpaid</option>
                                <option value="1">Paid</option>
                            </select>
                            @error('status')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        
                        <div class="form-group col-6">
                            <label for="">Total Amount</label>
                            <input type="number" class="form-control" name="total_amount" placeholder="Enter Total Amount" value="{{ old('total_amount') }}">
                            @error('total_amount')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group col-12">
                            <label for="">Invoice Data</label>
                            <div id="invoiceData">
                                <div class="row text-center">
                                    <div class="col">Date</div>
                                    <div class="col">Description</div>
                                    <div class="col">Amount</div>
                                    <div class="col-2"></div>
                                </div>
                                <div class="row my-2 tableRow">
                                    <div class="col">
                                        <input type="date" class="form-control rowDate" name="invoice_data[date][]" placeholder="Enter date">
                                    </div>
                                    <div class="col">
                                        <textarea class="form-control rowDescription" name="invoice_data[description][]" id="description" rows="1"></textarea>
                                    </div>
                                    <div class="col">
                                        <input type="number" class="form-control rowAmount" name="invoice_data[amount][]" placeholder="Enter Amount">
                                    </div>
                                    <div class="col-2">
                                        <a href="javascript:void" class="btn btn-md btn-primary mx-1 addData">+</a>
                                        <a href="javascript:void" class="btn btn-md btn-danger mx-1 removeData">-</a>
                                    </div>
                                </div>
                                <div class="row my-2 tableRow">
                                    <div class="col"><span class="text-danger date-error"></span></div>
                                    <div class="col"><span class="text-danger description-error"></span></div>
                                    <div class="col"><span class="text-danger amount-error"></span></div>
                                    <div class="col-2"></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    @section('script')
    <script>
        $(document).on('click','.addData',function () { 
            let clone = $(this).closest('.tableRow').clone();
            $(clone).find('input').val('');
            $(clone).find('textarea').val('');
            $(this).closest('.tableRow').after(clone);
        });
        $(document).on('click','.removeData',function () { 
            let tr = $('.tableRow').length;
            if(tr > 1){
                $(this).closest('.tableRow').remove();
            }
        });

        $(document).on('submit','form',function(){
            let form = $(this);
            let $input = $(form).find('#invoiceData').find('input');
            let $textarea = $(form).find('#invoiceData').find('textarea');
            let $error = 0;

            $date = $amount = $desc = "";
            $.each($input, function (index, tag) { 
                if(tag.value == ""){
                    if($(tag).hasClass('rowDate')){
                        $date = 'Date Required!';
                    }else if($(tag).hasClass('rowAmount')){
                        $amount = 'Amount Required!';
                    }
                    $error++;
                }
            });
            $.each($textarea, function (index, tag) { 
                if(tag.value == ""){                    
                    $desc = 'Description Required!';
                    $error++;
                    return false;
                }
            });
            
            $(form).find('.date-error').text($date);
            $(form).find('.amount-error').text($amount);
            $(form).find('.description-error').text($desc);

            if($error){
                $(form).find('[type="submit"]').prop('disabled',false);
                return false;
            }
            return true;
        });
    </script>
    @endsection
@endsection