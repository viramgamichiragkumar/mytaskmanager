@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __(config('moduleName')) }}
                    <div class="float-right">
                        <a href="{{ route(config('moduleUrl').".add") }}" class="btn btn-primary">Add</a>
                    </div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table server_table w-100">
                        <thead>
                            <tr>
                                <th width="10%">Sr no.</th>
                                <th>Invoice No.</th>
                                <th>Receiver Name</th>
                                <th>Title</th>
                                <th>Start Date</th>
                                <th>Due Date</th>
                                <th>Total Amount</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
    $(function () {
        let ajaxUrl =  "{{ route(config('moduleUrl')) }}";
        let cols = [
            {data: 'id', name: 'id'},
            {data: 'invoice_no', name: 'invoice_no'},
            {data: 'receiver_name', name: 'receiver_name'},
            {data: 'title', name: 'title'},
            {data: 'start_date', name: 'start_date'},
            {data: 'due_date', name: 'due_date'},
            {data: 'total_amount', name: 'total_amount'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ];
        server_datatable(ajaxUrl,cols);
    });
    </script>
@endsection
@endsection