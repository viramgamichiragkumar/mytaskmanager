@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('About Us') }}</div>


                    <form action="" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group d-flex">
                                <div class="col-6 p-0">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control" name="image" id="image"
                                        placeholder="Enter image" value="{{ $about->image ?? '' }}"
                                        accept="image/*">
                                    <input type="hidden" name="old_file" value="{{ $about->image ?? '' }}">
                                </div>
                                <div class="col-6">
                                    <img id="image-preview"
                                        src="{{ ($about->image && file_exists(storage_path('app/public/' . $about->image))) ? asset('storage/app/public/' . $about->image) : noimage() }}"
                                        alt="image Preview" height="100px" width="100px">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Vision</label>
                                <textarea class="form-control ckeditor" name="vision">{{ $about->vision ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Mission</label>
                                <textarea class="form-control ckeditor" name="mission">{{ $about->mission ?? '' }}</textarea>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
