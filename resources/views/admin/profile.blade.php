@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">

                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="{{ $user->image ? asset('/storage/app/profile/' . $user->image) : noimage() }}"
                                alt="profile picture">
                        </div>

                        <h3 class="profile-username text-center">{{ $user->name }}</h3>

                        <p class="text-muted text-center">Admin</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Services</b> <a class="float-right">{{ $services ?? 0 }}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Company</b> <a class="float-right">{{ $company ?? 0 }}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Inquiry</b> <a class="float-right">{{ $inquiry ?? 0 }}</a>
                            </li>
                        </ul>

                        <a href="{{ route('home') }}" class="btn btn-primary btn-block"><b>Home</b></a>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">About Me</h3>
                    </div>
                    <div class="card-body">
                        <strong><i class="fas fa-share"></i> Name</strong>
                        <span class="text-muted float-right">{{ $user->name }}</span>
                        <hr>
                        <strong><i class="fas fa-share"></i> Email</strong>
                        <span class="text-muted float-right">{{ $user->email }}</span>
                        <hr>
                        <strong><i class="fas fa-share"></i> Created On</strong>
                        <span class="text-muted float-right">{{ $user->created_at->Format('d/m/Y') }}</span>
                        <hr>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#account" data-toggle="tab">Account</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#activity" data-toggle="tab">Activity</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">

                            <div class="active tab-pane" id="account">
                                <form action="" id="profile" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="name" value="{{ $user->name }}"
                                                placeholder="Name">
                                            @error('name')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" name="email" value="{{ $user->email }}"
                                                placeholder="Email">
                                            @error('email')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="password" id="password"
                                                placeholder="password">
                                            @error('password')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Confirm Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="confirmpassword" id="confirmpassword"
                                                placeholder="Confirm password">
                                            <div class="password-error text-danger">
                                                @error('confirmpassword')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-2 col-form-label">Image</label>
                                        <div class="col-sm-7">
                                            <input type="file" class="form-control" name="image" id="image" placeholder="Enter image" accept="image/*">
                                            <input type="hidden" class="form-control" name="old_image" value="{{ $user->image }}">
                                        </div>
                                        <div class="col-3">
                                            <img id="image-preview" src="{{ ($user->image && file_exists(storage_path('app/profile/' . $user->image))) ? asset('storage/app/profile/'.$user->image) : noimage() }}" alt="image Preview" height="100px" width="100px">
                                        </div>
                                        @error('image')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    

                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane" id="activity">
                                <div class="timeline timeline-inverse">
                                    <div class="time-label">
                                        <span class="bg-danger">
                                            10 Feb. 2014
                                        </span>
                                    </div>
                                    <div>
                                        <i class="fas fa-envelope bg-primary"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="far fa-clock"></i> 12:05</span>

                                            <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email
                                            </h3>

                                            <div class="timeline-body">
                                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                                weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                                quora plaxo ideeli hulu weebly balihoo...
                                            </div>
                                            <div class="timeline-footer">
                                                <a href="#" class="btn btn-primary btn-sm">Read more</a>
                                                <a href="#" class="btn btn-danger btn-sm">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <i class="fas fa-user bg-info"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                                            <h3 class="timeline-header border-0"><a href="#">Sarah Young</a> accepted
                                                your friend request
                                            </h3>
                                        </div>
                                    </div>
                                    <div>
                                        <i class="fas fa-comments bg-warning"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="far fa-clock"></i> 27 mins ago</span>

                                            <h3 class="timeline-header"><a href="#">Jay White</a> commented on your
                                                post</h3>

                                            <div class="timeline-body">
                                                Take me to your leader!
                                                Switzerland is small and neutral!
                                                We are more like Germany, ambitious and misunderstood!
                                            </div>
                                            <div class="timeline-footer">
                                                <a href="#" class="btn btn-warning btn-flat btn-sm">View comment</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="time-label">
                                        <span class="bg-success">
                                            3 Jan. 2014
                                        </span>
                                    </div>
                                    <div>
                                        <i class="fas fa-camera bg-purple"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="far fa-clock"></i> 2 days ago</span>

                                            <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos
                                            </h3>

                                            <div class="timeline-body">
                                                {{-- <img src="https://placehold.it/150x100" alt="..."> --}}
                                                <img src="{{ noimage() }}" alt="...">
                                                {{-- <img src="https://placehold.it/150x100" alt="..."> --}}
                                                <img src="{{ noimage() }}" alt="...">
                                                {{-- <img src="https://placehold.it/150x100" alt="..."> --}}
                                                <img src="{{ noimage() }}" alt="...">
                                                {{-- <img src="https://placehold.it/150x100" alt="..."> --}}
                                                <img src="{{ noimage() }}" alt="...">
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <i class="far fa-clock bg-gray"></i>
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @section('script')
        <script>
            $('#image').change(function() {
                previewImage(this, '#image-preview');
            });
            $(document).on('submit','#profile',function(){
                $(this).find('.password-error').text("");
                if($('#password').val() != "" || $('#confirmpassword').val() != ""){
                    if($('#confirmpassword').val() !== $('#password').val()){
                        $(this).find('.password-error').text("Password doesn't match");
                        $(this).find('[type="submit"]').prop('disabled',false);
                        return false;
                    }else if($('#confirmpassword').val() == $('#password').val()){
                        if(!confirm('New Password Change will redirect you to re-login.')){
                            return false;
                        }
                    }
                }

                return true;
            })
        </script>
    @endsection
@endsection
