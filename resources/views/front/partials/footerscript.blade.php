    <!-- JS here -->
		<!-- All JS Custom Plugins Link Here here -->
        <script src="{{ asset('public/front/assets/js/vendor/modernizr-3.5.0.min.js') }}"></script>
		<!-- Jquery, Popper, Bootstrap -->
		<script src="{{ asset('public/front/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/popper.min.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/bootstrap.min.js') }}"></script>
	    <!-- Jquery Mobile Menu -->
        <script src="{{ asset('public/front/assets/js/jquery.slicknav.min.js') }}"></script>

		<!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="{{ asset('public/front/assets/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/slick.min.js') }}"></script>
		<!-- One Page, Animated-HeadLin -->
        <script src="{{ asset('public/front/assets/js/wow.min.js') }}"></script>
		<script src="{{ asset('public/front/assets/js/animated.headline.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/jquery.magnific-popup.js') }}"></script>

		<!-- Nice-select, sticky -->
        <script src="{{ asset('public/front/assets/js/jquery.nice-select.min.js') }}"></script>
		<script src="{{ asset('public/front/assets/js/jquery.sticky.js') }}"></script>
        
        <!-- contact js -->
        <script src="{{ asset('public/front/assets/js/contact.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/jquery.form.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/mail-script.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/jquery.ajaxchimp.min.js') }}"></script>
        
		<!-- Jquery Plugins, main Jquery -->	
        <script src="{{ asset('public/front/assets/js/plugins.js') }}"></script>
        <script src="{{ asset('public/front/assets/js/main.js') }}"></script>
        
        <script src="{{ asset('public/assets/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
