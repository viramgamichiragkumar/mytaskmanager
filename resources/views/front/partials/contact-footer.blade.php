@php
    $is_brand = isset($is_brand) ? $is_brand : true;
    $is_contact = isset($is_contact) ? $is_contact : true;
@endphp

@if($is_brand)
    <!-- Brand Area Start -->
    <div class="brand-area {{-- pb-bottom --}} my-5">
        <div class="container">
            <div class="brand-active brand-border pt-50 pb-40">
                <div class="single-brand">
                    <img src="{{ asset('public/front/assets/img/gallery/brand1.png') }}" alt="">
                </div>
                <div class="single-brand">
                    <img src="{{ asset('public/front/assets/img/gallery/brand2.png') }}" alt="">
                </div>
                <div class="single-brand">
                    <img src="{{ asset('public/front/assets/img/gallery/brand3.png') }}" alt="">
                </div>
                <div class="single-brand">
                    <img src="{{ asset('public/front/assets/img/gallery/brand4.png') }}" alt="">
                </div>
                <div class="single-brand">
                    <img src="{{ asset('public/front/assets/img/gallery/brand2.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Brand Area End -->
@endif

@if($is_contact)
<!-- Contact Info Start -->
<section class="contact-info-area w-padding2" data-background="{{ asset('public/front/assets/img/gallery/section_bg04.jpg') }}" id="contact-info-area">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-xl-5 col-lg-7">
              <div class="contact-caption mb-50">
                    <h3>If Not Now, When? Let’s Work Together!</h3>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor ididunt ut labore et dolore magna aliqua. Quis ipsum suspendisces gravida. Risus commodo viverra.</p>
                    <img src="{{ asset('public/front/assets/img/gallery/sin.png') }}" alt="">
              </div>
            </div>
            <div class="col-xl-5 col-lg-5">
                <form action="{{ route('inquiry_front') }}" method="POST" class="contact-wrapper">
                    @csrf
                    @error('name') <span>{{ $message }}</span> @enderror
                    <input type="text" name="name" placeholder="Full Name" value="{{ old('name') }}">

                    @error('email') <span>{{ $message }}</span> @enderror
                    <input type="email" name="email" placeholder="Email Address" value="{{ old('email') }}">
                    
                    @error('phone') <span>{{ $message }}</span> @enderror
                    <input type="number" name="phone" placeholder="Mobile Number" value="{{ old('phone') }}">

                    @error('subject') <span>{{ $message }}</span> @enderror
                    <input type="text" name="subject" placeholder="Subject" value="{{ old('subject') }}">

                    @error('message') <span>{{ $message }}</span> @enderror
                    <textarea name="message" id="message" placeholder="Your Message">{{ old('message') }}</textarea>
                    
                    <button class="submit-btn2" type="submit">Send Message</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Contact Info End -->
@endif