<!-- CSS here -->
<link rel="stylesheet" href="{{ asset('public/front/assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/slicknav.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/flaticon.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/fontawesome-all.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/themify-icons.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/slick.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('public/front/assets/css/style.css') }}">