<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ ucfirst(setting('sitename')) }}</title>
    <meta name="description" content="{{ ucfirst(setting('sitename')) }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{ (setting('favicon')) ? asset('/storage/app/setting/'.setting('favicon')) : asset('public/front/assets/img/favicon.ico') }}">

    @include('front.partials.headerscript')
    <style>
        .error{
            color:red;
        }
    </style>
</head>

<body>
    @include('front.partials.header')
    <main>

        @yield('content')

    </main>
    @include('front.partials.footer')
    @include('front.partials.footerscript')
</body>
@yield('script')

<script>
    
    var toastMixin = Swal.mixin({
        toast: true,
        icon: 'success',
        title: 'General Title',
        animation: false,
        position: 'top-right',
        showConfirmButton: false,
        timer: 4000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    
    @if (session()->has('success'))
        toastMixin.fire({
            animation: true,
            iconColor: 'white',
            customClass: {
                popup: 'colored-toast'
            },
            title: "{{ session()->get('success') }}",
        });

        // $(document).Toasts('create', {
        //     title: 'Success',
        //     body: '{{ session()->has('success') }}'
        // })

    @endif

    @if (session()->has('error'))
        toastMixin.fire({
            animation: true,
            iconColor: 'white',
            customClass: {
                popup: 'colored-toast'
            },
            icon: 'error',
            title: "{{ session()->get('error') }}",
        });
    @endif
</script>
</html>
