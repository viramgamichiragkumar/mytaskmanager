<footer>
    @php
        $contactUs = contactUs();
    @endphp
    <!-- Footer Start-->
    <div class="footer-area">
        <div class="container">
           <div class="footer-top footer-padding">
               <div class="row justify-content-center">
                   <div class="col-lg-6">
                        <div class="footer-top-cap text-center">
                            <img src="{{ (setting('logo')) ? asset('storage/app/setting/'.setting('logo')) : asset('public/front/assets/img/logo/logo2_footer.png') }}" alt="">
                            <span><a href="mailto:{{ $contactUs->email ?? '' }}">{{ $contactUs->email ?? '' }}</a></span>
                            <p>{{ $contactUs->address ?? '' }}</p>
                        </div>
                   </div>
               </div>
           </div>
            <div class="footer-bottom">
                <div class="row d-flex justify-content-between align-items-center">
                    <div class="col-xl-9 col-lg-8">
                        <div class="footer-copy-right">
                            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved <span style="visibility: hidden;"> | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></span>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4">
                        <!-- Footer Social -->
                        <div class="footer-social f-right">
                            <a>Stay Connected</a>
                            <a href="{{ setting('social')['facebook'] ?? '' }}"><i class="fab fa-facebook-f"></i></a>
                            <a href="{{ setting('social')['linkedin'] ?? '' }}"><i class="fas fa-globe"></i></a>
                            <a href="{{ setting('social')['instagram'] ?? '' }}"><i class="fab fa-instagram"></i></a>
                            <a href="{{ setting('social')['twitter'] ?? '' }}"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
    <!-- Footer End-->
</footer>
<!-- Scroll Up -->
<div id="back-top" style="z-index: 100;">
    <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
</div>
