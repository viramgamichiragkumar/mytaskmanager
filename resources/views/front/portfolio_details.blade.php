@extends('front.partials.app')
@section('content')

        @include('front.partials.breadcrumb',['page'=>'My Portfolio'])
        
        <!-- Services Details Start -->
        <div class="portfolio-details-area section-padding2">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="portfolio-img">
                            <div class="details-img mb-40">
                                <img src="{{ asset('public/front/assets/img/gallery/portfolio.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="details-caption pl-50">
                            <p>Mollit anim laborum.Dvcuis aute iruxvfg dhjkolohr in re voluptate velit esscillumlore eu quife nrulla parihatur. Excghcepteur sfwsignjnt occa cupidatat non aute iruxvfg dhjinulpadeserunt mollitemnth incididbnt ut;o5tu layjobore mofllit anim.Mollit anim laborum.Dvcuis aute iruxvfg.</p>

                            <p class="mb-50">Mollit anim laborum.Dvcuis aute iruxvfg dhjkolohr in re voluptate velit esscillumlore eu quife nrulla parihatur. Excghcepteur sfwsignjnt occa cupidatat non aute iruxvfg dhjinulpadeserunt mollitemnth incididbnt ut;o5tu layjobore mofllit anim.Mollit anim laborum.Dvcuis aute iruxvfg.</p>

                            <h3>How can we help?</h3>
                            <p>Mollit anim laborum.Dvcuis aute iruxvfg dhjkolohr in re voluptate velit esscillumlore eu quife nrulla parihatur. Excghcepteur sfwsignjnt occa cupidatat non aute iruxvfg dhjinulpadeserunt mollitemnth incididbnt ut;o5tu layjobore mofllit anim.Mollit anim laborum.Dvcuis aute iruxvfg.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Services Details End -->
        
@include('front.partials.contact-footer',['is_brand'=>false])

@endsection