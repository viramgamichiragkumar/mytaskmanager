@extends('front.partials.app')
@section('content')
   
    @include('front.partials.breadcrumb',['page'=>'About me'])

    <!-- About Me Start -->
    <div class="about-me pb-top">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="about-me-img mb-30">
                        <img src="{{ ($data->image) ? asset('storage/app/public/'.$data->image) : asset('public/front/assets/img/gallery/aboutme.png') }}" alt="">
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    @if($data->vision)
                        {!! $data->vision !!}
                    @else
                        <div class="about-me-caption">
                            <h2>I Create Products Not Just Art.</h2>
                            <p class="pb-30">Unlimited rewards. enjoy attractive discounts flexible Payme options global usage.
                                Unlimited rewards. enjoy attracti exible ayment options global usage.</p>
                            <h5>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.</h5>
                            <p>Unlimited rewards. enjoy attractive discounts flexible Payme options global usage. Unlimited
                                rewards. enjoy attracti exible ayment options global usage.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- About Me End -->
    <!-- About Area start -->
    <section class="about-area section-paddingt30">
        <div class="container">
            <div class="row ">
                @if($data->mission)
                    {!! $data->mission !!}
                @else
                    <div class="col-lg-5">
                        <div class="about-caption mb-50">
                            <h3>Designing With Passion While Exploring The World.</h3>
                            <p class="pera1">Consectetur adipiscing elit, sed do eiusmod tempor ididunt ut labore et dolore
                                magna aliqua. Quis ipsum suspendisces gravida. Risus commodo viverra sebfd dho eiusmod tempor
                                maecenas accumsan lacus.</p>
                            <p>Consectetur adipiscing elit, sed do eiusmod tempor ididunt ut labore et dolore magna aliqua. </p>
                        </div>
                    </div>
                @endif
                <div class="col-lg-5 offset-lg-1">
                    <div class="about-caption2">
                        <h3>Any Type Of Query<br> & Discussion.</h3>
                        <p>Late talk with me</p>
                        <div class="send-cv">
                            @php($email = contactUs('email'))
                            <a href="mailto:{{ $email }}">{{ $email }}</a>
                            <i class="ti-arrow-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Area End -->
   
    <x-contact-form />
    

    <!-- client-comments -->
    <div class="client-comments section-paddingt30">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-8 col-md-10">
                    <!-- Section Tittle -->
                    <div class="section-tittle mb-70">
                        <h2>Some Possitive Feedback That Encourage Us</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- latest-blog-area start -->
        <div class="latest-blog-area">
            <div class="container">
                <div class="custom-row">
                    <div class="blog-active">
                        <!-- single-items -->
                        <div class="col-xl-4">
                            <div class="blog-wrapper">
                                <div class="blog-inner">
                                    <div class="blog-top">
                                        <div class="person-img">
                                            <img src="{{ asset('public/front/assets/img/gallery/blog1.png') }}" alt="">
                                        </div>
                                        <div class="comment-person">
                                            <h2>Bradley Erickson</h2>
                                            <span>UI/UX Designer</span>
                                        </div>
                                    </div>
                                    <p>Consectetur adipisicing elit, seddosdoe eiusmod tempor incididunt utore etstes dolore
                                        magna aliqua. Ut imminim restai veniam, quis nostrud.</p>
                                </div>
                            </div>
                        </div>
                        <!-- single-items -->
                        <div class="col-xl-4">
                            <div class="blog-wrapper">
                                <div class="blog-inner">
                                    <div class="blog-top">
                                        <div class="person-img">
                                            <img src="{{ asset('public/front/assets/img/gallery/blog2.png') }}" alt="">
                                        </div>
                                        <div class="comment-person">
                                            <h2>Bradley Erickson</h2>
                                            <span>UI/UX Designer</span>
                                        </div>
                                    </div>
                                    <p>Consectetur adipisicing elit, seddosdoe eiusmod tempor incididunt utore etstes dolore
                                        magna aliqua. Ut imminim restai veniam, quis nostrud.</p>
                                </div>
                            </div>
                        </div>
                        <!-- single-items -->
                        <div class="col-xl-4">
                            <div class="blog-wrapper">
                                <div class="blog-inner">
                                    <div class="blog-top">
                                        <div class="person-img">
                                            <img src="{{ asset('public/front/assets/img/gallery/blog3.png') }}" alt="">
                                        </div>
                                        <div class="comment-person">
                                            <h2>Bradley Erickson</h2>
                                            <span>UI/UX Designer</span>
                                        </div>
                                    </div>
                                    <p>Consectetur adipisicing elit, seddosdoe eiusmod tempor incididunt utore etstes dolore
                                        magna aliqua. Ut imminim restai veniam, quis nostrud.</p>
                                </div>
                            </div>
                        </div>
                        <!-- single-items -->
                        <div class="col-xl-4">
                            <div class="blog-wrapper">
                                <div class="blog-inner">
                                    <div class="blog-top">
                                        <div class="person-img">
                                            <img src="{{ asset('public/front/assets/img/gallery/blog2.png') }}" alt="">
                                        </div>
                                        <div class="comment-person">
                                            <h2>Bradley Erickson</h2>
                                            <span>UI/UX Designer</span>
                                        </div>
                                    </div>
                                    <p>Consectetur adipisicing elit, seddosdoe eiusmod tempor incididunt utore etstes dolore
                                        magna aliqua. Ut imminim restai veniam, quis nostrud.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End latest-blog-area -->
    </div>
@include('front.partials.contact-footer')

@endsection
