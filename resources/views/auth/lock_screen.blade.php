@extends('layouts.app')

@section('content')

    <body class="hold-transition lockscreen">

        <div class="lockscreen-wrapper">
            <div class="lockscreen-logo">
                <a href="../../index2.html"><b>{{ config('app.name') }}</b></a>
            </div>

            {{-- <div class="lockscreen-name">{{ auth()->user()->name }}</div> --}}
            <div class="lockscreen-name">Lock Screen</div>

            <div class="lockscreen-item">

                <div class="lockscreen-image">
                    <img src="{{ (auth()->user()->image) ? url('storage/app/profile/'.auth()->user()->image) : noimage(1) }}" alt="User Image">
                </div>


                <form class="lockscreen-credentials" method="POST">
                    @csrf
                    <div class="input-group">
                        <input type="password" name="password" class="form-control" placeholder="password">
                        <div class="input-group-append">
                            <button type="submit" class="btn">
                                <i class="fas fa-arrow-right text-muted"></i>
                            </button>
                        </div>
                    </div>
                </form>

            </div>

            <div class="help-block text-center">
                Enter your password to retrieve your session
            </div>
            <div class="text-center">
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ __('Or sign in as a different user') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    
            </div>
            <div class="lockscreen-footer text-center">
                Copyright © {{ date('Y') }} <b><a href="{{ url('/') }}" class="text-black">{{ config('app.name') }}</a></b><br>
                All rights reserved
            </div>
        </div>


        <script src="../../plugins/jquery/jquery.min.js"></script>

        <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


    </body>
@endsection
