<!DOCTYPE html>
<html>

<head>
    <title>My Portfolio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Custom CSS -->
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
            background-color: #f5f5f5;
        }

        .navbar {
            background-color: #333;
        }

        .navbar-brand {
            color: #fff;
        }

        .nav-link {
            color: #fff;
        }

        .jumbotron {
            background-image: url('https://picsum.photos/1920/1080');
            background-size: cover;
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            color: #fff;
            text-align: center;
            margin-bottom: 0;
        }

        .jumbotron h1 {
            font-size: 5rem;
            margin-bottom: 1rem;
        }

        .jumbotron p {
            font-size: 2rem;
            margin-bottom: 3rem;
        }

        .btn-primary {
            background-color: #333;
            border-color: #333;
        }

        .btn-primary:hover {
            background-color: #fff;
            color: #333;
        }

        .section-heading {
            margin-top: 3rem;
            margin-bottom: 3rem;
            text-align: center;
        }

        .portfolio-item {
            margin-bottom: 3rem;
            cursor: pointer;
        }

        .portfolio-item img {
            max-width: 100%;
            transition: transform 0.2s ease-in-out;
        }

        .portfolio-item img:hover {
            transform: scale(1.1);
        }

        .contact-form {
            margin-top: 3rem;
        }

        .form-control:focus {
            box-shadow: none;
        }

        .footer {
            background-color: #333;
            color: #fff;
            padding-top: 3rem;
            padding-bottom: 3rem;
        }

        .footer ul {
            list-style: none;
            padding-left: 0;
        }

        .footer ul li {
            margin-bottom: 1rem;
        }

        .footer a {
            color: #fff;
        }
    </style>
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="#">My Portfolio</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link"
                            href="#about
                        About</a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#portfolio">Portfolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Jumbotron -->
    <section id="jumbotron" class="jumbotron">
        <div class="container">
            <h1>Welcome to My Portfolio</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod tellus quis massa consequat, ut
                bibendum eros maximus.</p>
            <a href="#portfolio" class="btn btn-lg btn-primary">View Portfolio</a>
        </div>
    </section>

    <!-- About -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>About Me</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod tellus quis massa
                        consequat, ut bibendum eros maximus. Donec vel felis tortor. Suspendisse tristique vel sapien
                        non tincidunt. Vestibulum eleifend varius orci, vel volutpat leo vestibulum sed.</p>
                </div>
                <div class="col-md-6">
                    <h2>Skills</h2>
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 90%;" aria-valuenow="90"
                            aria-valuemin="0" aria-valuemax="100">HTML/CSS</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 80%;" aria-valuenow="80"
                            aria-valuemin="0" aria-valuemax="100">JavaScript</div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: 70%;" aria-valuenow="70"
                            aria-valuemin="0" aria-valuemax="100">PHP</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Portfolio -->
    <section id="portfolio">
        <div class="container">
            <h2 class="section-heading">Portfolio</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="portfolio-item">
                        <img src="https://picsum.photos/600/400?random=1" alt="Portfolio Item">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="portfolio-item">
                        <img src="https://picsum.photos/600/400?random=2" alt="Portfolio Item">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="portfolio-item">
                        <img src="https://picsum.photos/600/400?random=3" alt="Portfolio Item">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="portfolio-item">
                        <img src="https://picsum.photos/600/400?random=4" alt="Portfolio Item">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="portfolio-item">
                        <img src="https://picsum.photos/600/400?random=5" alt="Portfolio Item">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="portfolio-item">
                        <img src="https://picsum.photos/600/400?random=6" alt="Portfolio Item">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact -->
    <section id="contact">
        <div class="container">
            <h2 class="section-heading">Contact</h2>
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter your name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" aria-describedby="emailHelp"
                                placeholder="Enter your email address">
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" id="message" rows="3" placeholder="Enter your message"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="contact-info">
                        <h3>Contact Information</h3>
                        <p><i class="fas fa-map-marker-alt"></i> 123 Main St, New York, NY 10001</p>
                        <p><i class="fas fa-phone"></i> (123) 456-7890</p>
                        <p><i class="fas fa-envelope"></i> email@example.com</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer>
        <div class="container">
            <p>&copy; 2023 My Portfolio. All Rights Reserved.</p>
        </div>
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>

</html>
