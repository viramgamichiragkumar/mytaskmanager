<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InquiryController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear_command', function () {
    Artisan::call('route:clear');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    // Artisan::call('package:discover --ansi');
    return back();
});



Route::prefix('/')->group(function(){

    Route::get('/', [FrontController::class,'index']);
    Route::get('contactus',[FrontController::class,'contactus'])->name('contactus');
    Route::get('about',[FrontController::class,'about'])->name('about');
    Route::get('services',[FrontController::class,'services'])->name('services');
    Route::view('portfolio','front.portfolio')->name('portfolio');
    Route::view('portfolio_details','front.portfolio_details')->name('portfolio_details');
    
    Route::post('inquiry',[InquiryController::class,'inquiry_front'])->name('inquiry_front');
});

Route::prefix('/admin')->group(function(){

    Route::get('/',function(){
        return redirect()->route((Auth::check()) ? 'home' : 'login');
    });

    Route::get('/login/google', [LoginController::class, 'redirectToGoogle'])->name('login.google');
    Route::get('/login/google/callback', [LoginController::class, 'handleGoogleCallback'])->name('login.google.callback');
    Route::get('/login/facebook/callback', [LoginController::class,'handleFacebookCallback'])->name('login.facebook');

    Auth::routes();

    Route::middleware(['auth'])->group(function () {
        Route::any('/lock', [HomeController::class, 'lock_screen'])->name('lock-screen');
        Route::middleware(['auto.lock'])->group(function () {
            Route::get('/home', [HomeController::class, 'index'])->name('home');
            Route::any('/profile', [HomeController::class, 'profile'])->name('admin.profile');
            Route::any('/contactus',[HomeController::class,'contactus'])->name('admin.contactus');
            Route::any('/aboutus',[HomeController::class,'aboutus'])->name('admin.aboutus');
            Route::any('/setting',[SettingController::class,'index'])->name('admin.setting');
            
            Route::get('/portfolio',[PortfolioController::class,'index'])->name('admin.portfolio');
            Route::post('/portfolio/update',[PortfolioController::class,'portfolio'])->name('admin.portfolio.update');
            

            Route::prefix('services')->name('admin.')->controller(ServiceController::class)->group(function(){
                Route::get('/','index')->name('services');
                Route::get('/add','add')->name('services.add');
                Route::post('/store','store')->name('services.store');
                Route::get('/edit/{id}','edit')->name('services.edit');
                Route::post('/update','update')->name('services.update');
                Route::post('/delete/{id}','delete')->name('services.delete');
            });

            Route::prefix('company')->name('admin.')->controller(CompanyController::class)->group(function(){
                Route::get('/','index')->name('company');
                Route::get('/add','add')->name('company.add');
                Route::post('/store','store')->name('company.store');
                Route::get('/edit/{id}','edit')->name('company.edit');
                Route::post('/update','update')->name('company.update');
                Route::post('/delete/{id}','delete')->name('company.delete');
            });

            Route::prefix('inquiry')->name('admin.')->controller(InquiryController::class)->group(function(){
                Route::get('/','index')->name('inquiry');
                Route::get('/add','add')->name('inquiry.add');
                Route::post('/store','store')->name('inquiry.store');
                Route::get('/edit/{id}','edit')->name('inquiry.edit');
                Route::post('/update','update')->name('inquiry.update');
                Route::post('/delete/{id}','delete')->name('inquiry.delete');
            });

            Route::prefix('invoice')->name('admin.')->controller(InvoiceController::class)->group(function(){
                Route::get('/','index')->name('invoice');
                Route::get('/add','add')->name('invoice.add');
                Route::post('/store','store')->name('invoice.store');
                Route::get('/edit/{id}','edit')->name('invoice.edit');
                Route::post('/update','update')->name('invoice.update');
                Route::post('/delete/{id}','delete')->name('invoice.delete');
                Route::get('/download_invoice/{id}','download_invoice')->name('invoice.download');
            });



        });
    });


});
