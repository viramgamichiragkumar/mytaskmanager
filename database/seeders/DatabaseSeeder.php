<?php

namespace Database\Seeders;

use App\Models\Portfolio;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        if(!User::where('email','admin@gmail.com')->exists()){
            User::create([
                'name' =>'admin',
                'email' =>'admin@gmail.com',
                'password' => Hash::make('123'),
            ]);
        }
        if(!Portfolio::where('id',1)->exists()){
            $aboutme = "Step into the realm of innovation with me, Chirag Viramgami, an experienced IT professional, and dedicated web development enthusiast! With a solid two-year background in PHP and mastery of the dynamic Laravel framework, I take immense pride in transforming ideas into reality through elegant and user-centric web solutions. My diverse portfolio showcases stunning web projects that reflect my commitment to excellence.

            Beyond my technical expertise, I find great fulfillment in mentoring and nurturing the next generation of IT talent. Aspiring IT newbies and junior developers benefit from my interactive training sessions and inspiring internships, embarking on a journey of growth and success under my expert guidance.
            
            Together, we'll embark on an exciting journey, exploring the latest technologies and unleashing creativity to enhance your online presence. Working as a team, we'll turn your digital dreams into reality, paving the way for a brighter future and remarkable business growth!";

            Portfolio::updateOrCreate(['id' => 1], [
                'name' => "Chirag Viramgami",
                'slug' => \Str::slug("Chirag Viramgami"),
                'designation' => "Web Developer",
                'aboutme' => $aboutme,
            ]);
            
        }
    }
}
